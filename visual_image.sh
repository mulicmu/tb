#!/bin/bash

nimage_per_page=50
npage=5

root=www
rm -rf $root
mkdir -p $root
index=$root/index.html
iagent=0
for file in image/emer*; do
    agent=$(basename $file)
    list=output/${agent}_0_list

    for ((i = 0; i < npage; ++i)); do
        html=$root/agent_${iagent}_pg_$i.html
        grep txt $list | shuf | head -n $nimage_per_page | \
            while read line; do
            id=`echo $line | awk 'BEGIN{FS="[ \.]"} {print $9}'`

            txt=`cat $file/0/${id}.txt | awk 'BEGIN{FS="\",\""}{print $6}'`
            echo "<p> ${txt:0:${#txt}-1} <br><br>" >>$html


            grep "${id}.*jpg" $list | while read jpg; do
                jfile=`echo $jpg | awk '{print $9}'`
                dfile=file/$jfile
                cp $file/0/$jfile $root/$dfile
                echo "<a href=\"${dfile}\"> <img src=\"${dfile}\" height=\"150px\"/></a>" >>$html
            done
            echo "</p>" >>$html
        done
        echo "<a href=\"agent_${iagent}_pg_$i.html\"> agent $iagent, page $i </a>" >>$index
    done

    iagent=$((iagent +1))
done
