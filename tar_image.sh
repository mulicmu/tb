#!/bin/bash

cur=`pwd`
out=$cur/tar-image/
for dir in image/*; do
    agent=$(basename $dir)
    cd $dir
    mkdir -p $out/$agent
    for p in *; do
        echo $dir/$p;
        tar cf $out/$agent/$p.tar $p
    done
    cd $cur
done
