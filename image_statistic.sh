#!/bin/bash

printf "%20s %10s %10s %14s\n" "agent" "#listing" "#image" "size (GB)"
mkdir -p output
for file in image/*; do
    agent=$(basename $file)
    list=output/${agent}_0_list
    if [ ! -s $list ]; then
        ls -alF image/${agent}/0 >$list
    fi
    txt=`grep txt $list | wc -l`
    jpg=`grep jpg $list | wc -l`
    size=`awk '{s+=$5} END {print s/1e9*16}' $list`
    txt=$(($txt * 16))
    jpg=$(($jpg * 16))
    printf "%20s %10d %10d %14f\n" "${agent}" "${txt}" "${jpg}" "${size}"
done
