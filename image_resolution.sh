#!/bin/bash

# set -x
nfile=2000
for file in image/*; do
    agent=$(basename $file)
    list=output/${agent}_0_list
    out=output/${agent}_0_jpg_resolution
    rm -rf $out
    grep jpg $list | shuf | head -n $nfile | \
    while read line; do
        jpg=`echo $line | awk '{print $9}'`
        exiv2 -q image/$agent/0/$jpg | grep "Image size" | awk '{print $4, $6}' >>$out
    done
done
