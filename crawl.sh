#!/bin/bash
if [ $# -ne 3 ]; then
    echo "usage: $0 job start end"
    exit -1;
fi
start=$2
end=$3
out=image/$1
for i in {0..9}; do
    mkdir -p $out/$i
done
for i in {a..f}; do
    mkdir -p $out/$i
done

in=data/${1^^}.csv
# format: "MLS_NUMBER","STATUS","LATITUDE","LONGITUDE","URL","DESCRIPTION"
head -n $end $in | tail -n $(($end-$start)) | \
while read line; do
    id=`echo $line | awk 'BEGIN{FS="\",\""}{print $1}'`
    id=${id:1:${#id}-1}

    idx=0
    fin=0
    bin=`echo $id | md5sum`
    bin=${bin:2:1}
    allurl=`echo $line | awk 'BEGIN{FS="\",\""}{print $5}'`
    urls=(`echo $allurl | awk 'BEGIN{FS="\\\^=\\\^"}{for (i = 0; ++i <= NF;) print $i}'`)
    for url in ${urls[@]}
    do
        # echo $url
        file="${out}/${bin}/${id}_${idx}.jpg"
        if [ ! -s $file ]; then
            wget -q -O $file $url
            if [ $? != 0 ]; then
                echo "fail to get $url"
                rm -f $file
            else
                fin=$(($fin+1))
            fi
        fi
        idx=$(($idx + 1))
    done
    if [ $fin -gt 0 ]; then
        echo $line >"${out}/${bin}/$id.txt"
    fi
done
