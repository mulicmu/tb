#!/bin/bash
njob=80
nthread=30

agent=(smith atproperties alfarealty beangroup bobparks cbcaine emerge ifremont
 nathangrace premier pridemore selectgroup snyder sothebyma
 biltmore c21vjf cbpelican garygreene laffey pacunion
 preproperties rinehart sonoma southcal)

# generate job file
rm -rf jobs
mkdir -p log
for ((k = 0; k < 25; ++k)); do
    job=${agent[$k]}
    in=data/${job^^}.csv
    nline=`cat $in | wc -l`

    for ((i = 0; i < $njob; ++i)); do
        start=$(($nline/$njob*$i+1))
        end=$(($nline/$njob*($i+1)))
        echo "./crawl.sh $job $start $end >>log/${job}.log" >>jobs
    done
done

# crawl
parallel -j $nthread <jobs
